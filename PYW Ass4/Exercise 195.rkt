;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname |Exercise 195|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f ())))
(require 2htdp/universe)
(require 2htdp/image)

; Exercise 195


; A Transition.v2 is a structure:
;   (make-ktransition FSM-State KeyEvent FSM-State)

; A FSM is one of:
; -empty
; -(cons Trans FSM)
(define-struct ktrans [current ke next])

; A Transition is 
;   (make-transition FSM-State FSM-State)
 
; FSM-State is a String that specifies a color. 
 
; interpretation A FSM represents the transitions that a
; finite state machine can take from one state to another 
; in reaction to key strokes 

; SimulationState KeyEvent -> SimulationState
; responds on a key stroke ke and current simulationstate by finding the next
; state
(define FSmachine
  (list (make-ktrans "AA" "a" "BC")
        (make-ktrans "BC" "b" "BC")
        (make-ktrans "BC" "c" "BC")
        (make-ktrans "BC" "d" "DD")))

(define (main ws) 
  (big-bang ws
            (on-key find)
            (to-draw render)))

; KeyEvent SimulationState -> SimulationState
(define (find ke ss)
  (cond 
    [(empty? FSmachine) empty]
    [(and (equal? ke (ktrans-ke (first FSmachine)))
          (equal? (ktrans-current (first FSmachine)) ss))
     (ktrans-next (first FSmachine))]
    [(and (or (equal? ke (ktrans-ke (second FSmachine)))
              (equal? ke (ktrans-ke (third FSmachine))))
          (equal? (ktrans-current (second FSmachine)) ss))
     (ktrans-next (second FSmachine))]
    [(and (equal? ke (ktrans-ke (fourth FSmachine)))
          (equal? (ktrans-current (fourth FSmachine)) ss))
     (ktrans-next (fourth FSmachine))]
    [(and (string? ke)
          (equal? ss (ktrans-current (fifth FSmachine)))) 
                  (ktrans-next (fifth FSmachine))]))

     
(check-expect (find "a" "AA") "BC")
(check-expect (find "b" "BC") "BC")
(check-expect (find "d" "BC") "DD")

; KeyEvent SimulationState -> SimulationState
(define (render ss)
   (cond 
     ((equal? ss "AA") (square 100 "solid" "white"))
     ((equal? ss "BC") (square 100 "solid" "yellow"))
     ((equal? ss "DD") (square 100 "solid" "green"))
     ((equal? ss "ER") (square 100 "solid" "red"))))


