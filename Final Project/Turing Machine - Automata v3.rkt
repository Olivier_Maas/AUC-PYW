#lang racket
(require htdp/testing)

(define current-pos 1)
(define-syntax automaton
  (syntax-rules (:)
    [(_ init-state
        (state : response ...)
        ...)
     (let-syntax
         ([process-state
           (syntax-rules (accept ->)
             [(_ accept)
              (lambda (vec)
                (cond
                  [(< current-pos (sub1 (vector-length vec))) 
                   (begin (set! current-pos 1) true)]
                  [else (begin (set! current-pos 1) false)]))]
             [(_ (old-c -> new-c new-s move) (... ...))
              (lambda (vec)
                (cond
                  [(> current-pos (sub1 (vector-length vec))) 
                   (begin (set! current-pos 1) false)]
                  [else
                   (case (vector-ref vec current-pos)
                     [(old-c) (begin (vector-set! vec current-pos new-c)
                                     (if (equal? 'R move)
                                         (set! current-pos (add1 current-pos))
                                         (set! current-pos (sub1 current-pos)))
                                     (new-s vec))]
                     (... ...)
                     [else 
                      (begin (set! current-pos 1) false)])]))])])
       (letrec ([state
                 (process-state response ...)]
                ...)
         init-state))]))


(define tm1
  (automaton q0
             [q0 : (a -> 1 q1 'R)]
             [q1 : (a -> 1 q1 'R)
                   (b -> 1 q1 'L)
                   (1 -> 1 qf 'L)]
             [qf : accept]))


(define tm2
  (automaton q0
             [q0 : (a -> 'a q0 'R)
                 (b -> 'b q0 'R)
                 (c -> 'c q0 'R)
                 (e -> 'e q0 'R)
                 (0 -> 0 q1 'L)
                 (d -> 'd q1 'L)
                 (f -> 'f q1 'L)]
             [q1 : (a -> 'd q2 'L)
                 (b -> 'f q2 'L)
                 (c -> 'c q4 'R)
                 (e -> 'e q4 'R)]
             [q2 : (a -> 'a q2 'L)
                 (b -> 'b q2 'L)
                 (d -> 'd q2 'L)
                 (f -> 'f q2 'L)
                 (c -> 'c q3 'R)
                 (e -> 'e q3 'R)
                 (0 -> 0 q3 'R)]
             [q3 : (a -> 'c q0 'R)
                 (b -> 'e q0 'R)
                 (d -> 'd q4 'L)
                 (f -> 'f q4 'L)]
             [q4 : (1 -> 1 q4 'L)
                 (c -> 'c q4 'L)
                 (d -> 'd q4 'L)
                 (e -> 'e q4 'L)
                 (f -> 'f q4 'L)
                 (0 -> 0 q5 'R)]
             [q5 : (1 -> 1 q5 'R)
                 (c -> 1 q6 'R)
                 (e -> 1 q7 'R)
                 (0 -> 0 qf 'L)]
             [q6 : (1 -> 1 q6 'R)
                 (c -> 'c q6 'R)
                 (e -> 'e q6 'R)
                 (f -> 'f q6 'R)
                 (d -> 1 q4 'L)]
             [q7 : (1 -> 1 q7 'R)
                 (c -> 'c q7 'R)
                 (d -> 'd q7 'R)
                 (e -> 'e q7 'R)
                 (f -> 1 q4 'L)]
             [qf : accept]))


(check-expect (tm1 (vector 0 'a 1 'a 0)) #t)
(check-expect (tm1 (vector 0 'a 'a 0)) #f)
(check-expect (tm2 (vector 0 'a 'a 'b 'b 'a 'a 'b 'b 0)) #t)
(check-expect (tm2 (vector 0 'a 'a 'b 'b 'a 'a 'b 'a 0)) #f)


(test)