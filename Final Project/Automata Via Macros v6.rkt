#lang racket

(define-syntax automaton
  (syntax-rules (:)
    [(_ init-state
        (state : response ...)
        ...)
     (let-syntax
         ([process-state
           (syntax-rules (accept ->)
             [(_ accept)
              (lambda (stream)
                (cond
                  [(empty? stream) true]
                  [else false]))]
             [(_ (label -> target) (... ...))
              (lambda (stream)
                (cond
                  [(empty? stream) false]
                  [else
                   (case (first stream)
                     [(label) (target (rest stream))]
                     (... ...)
                     [else false])]))])])
       (letrec ([state
                 (process-state response ...)]
                ...)
         init-state))]))


(define m2
  (automaton init
             [init : (c -> more)]
             [more : (a -> more)
                   (d -> more)
                   (r -> end)]
             [end : accept]))
