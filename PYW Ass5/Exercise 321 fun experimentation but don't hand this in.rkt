;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname |Exercise 321 fun experimentation but don't hand this in|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
; Exercise 321

; [List-of Symbols] [List-of Numbers]
; Consumes a list of symbols and a list of numbers 
; and produces all possible ordered pairs of symbols and numbers.
(define (cross los lon)
  (map (lambda (s) (map (lambda (n) (list s n)) lon)) los))

(define (crosser1 los lon)
  (map (lambda (s n) (list s n)) los lon))

(define (crosser2 los lon)
  (cond
    [(empty? (rest los)) los]
    [else (append (map (lambda (s n) (list s n)) los lon) (crosser2 (rest los) lon))]))

(define (crosser3 los lon)
   (cond
     [(empty? los) los]
     [else (append (map (lambda (n) (list (first los) n)) lon) 
                   (crosser3 (rest los) lon))]))

; Remove outer list:
; (define (cross los lon)
;   (map (lambda (s) (map (lambda (n) (list s n)) lon)) los))