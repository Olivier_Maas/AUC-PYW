;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname |Exercise 259 - v3|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
; Exercise 259

; An S-expr (S-expression) is one of: 
; – Atom
; – SL
; An SL (S-list) is one of: 
; – empty
; – (cons S-expr SL)
; An Atom is one of: 
; – Number
; – String
; – Symbol 


; S-expr -> N 
; Calculates the depth of an S-expr.
(define (depth sexp)
  (cond
    [(atom? sexp) 1]
    [else (add1 (depth-sl sexp))]))

; SL -> N 
; Calculates the depth of an SL.
(define (depth-sl sl)
  (cond
    [(empty? sl) 0]
    [else (local ((define dfsl (depth (first sl)))
             (define dslrsl (depth-sl (rest sl))))
     (cond
            ((> dfsl dslrsl) dfsl)
            (else dslrsl)))]))

; S-expr -> Boolean
; Checks whether an S-expr is an Atom.
(define (atom? sexp)
  (cond
    ((or (number? sexp) (string? sexp) (symbol? sexp)) #t)
    (else #f)))

(check-expect (depth 'Atom) 1)
(check-expect (depth '((Hello) (Hello))) 3)
(check-expect (depth '(((Atom)) (Atom))) 4)
(check-expect (depth '(((Atom) (Atom)) (((Atom)) (Atom)))) 5)
