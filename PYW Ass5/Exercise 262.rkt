;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-intermediate-reader.ss" "lang")((modname |Exercise 262|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
; Exercise 262

; An S-expr (S-expression) is one of: 
; – Any
; – [list-of S-expr]

; S-expr Symbol -> N 
; counts all occurrences of sy in sexp.
(define (count sexp sy)
  (cond
    [(equal? sexp sy) 1]
    [(empty? sexp) 0]
    [(cons? sexp) (+ (count (first sexp) sy) (count (rest sexp) sy))]
    [else 0]))

(check-expect (count 'world 'hello) 0)
(check-expect (count '(world hello) 'hello) 1)
(check-expect (count '(((world) hello) hello) 'hello) 2)
