;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname legal) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f ())))
(define-struct possn [x y])

(define (legal alop)
  (cond
    [(empty? alop) empty]
    [(legalizer (first alop)) (cons (first alop) (legal (rest alop)))]
    [else (legal (rest alop))]))

(define (legalizer a-possn)
  (cond
    [(and (< 0 (possn-x a-possn) 100) (< 0 (possn-y a-possn) 200)) #t]
    [else #f]))

(check-expect 
 (legal (cons (make-possn 200 199) 
              (cons (make-possn 15 20) 
                    (cons (make-possn 100 200) empty)))) 
 (list (make-possn 15 20)))