;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-advanced-reader.ss" "lang")((modname from-sym) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #t #t none #f ())))
;; A List-of-Symbols is one of:
;; - empty, or
;; - (cons s los)
;; where s is a Symbol and los is a List-of-Symbols.

;; Symbol List-of-Symbols -> List-of-Symbols OR false
(define (from-sym s los)
  (cond
    [(empty? los) false]
    [(cons? los)
     (cond
       [(not (equal? s (first los))) (from-sym s (rest los))]
       [else (cons (first los) (rest los))])]))

(check-expect (from-sym 'f '(a b c d e f g h i)) '(f g h i))