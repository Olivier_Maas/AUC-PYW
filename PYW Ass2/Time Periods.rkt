;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname |Time Periods|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;Time periods
;One & Two -> Number
;Consumes timestamps "one" and "two" and counts the minutes between them, 
;with seconds neglected and rounded up to the next minute. 
;If two is earlier than one, the function will assume that midnight passed 
;between one and two, and count the minutes between them as such.
;If either timestamp is incorrect, the function will say so 
;and refuse to calculate a result.

(define-struct one [h m s])
(define-struct two [H M S])

(define ex1
  (make-one 23 57 30))
(define ex2
  (make-two 00 03 00))

(define ex3
  (make-one 00 03 00))
(define ex4
  (make-two 23 57 30))

(define ex5
  (make-one 60 70 80))


(check-expect (time-periods ex5 ex2) "Please input correct timestamps.")
(check-expect (time-periods ex1 ex2) 5)
(check-expect (time-periods ex3 ex4) 1435)

(define (time-periods a-one a-two) 
  (cond
    [(and (integer? (one-h a-one)) (integer? (one-m a-one)) 
          (integer? (one-s a-one))
          (<= 0 (one-h a-one) 23) (<= 0 (one-m a-one) 59) 
          (<= 0 (one-s a-one) 59)
          (integer? (two-H a-two)) (integer? (two-M a-two)) 
          (integer? (two-S a-two)) 
          (<= 0 (two-H a-two) 23) (<= 0 (two-M a-two) 59) 
          (<= 0 (two-S a-two) 59))
     (cond
          [(<= 0         (- (+ (* 60 (two-H a-two)) (two-M a-two) 
                               (cond[(> (two-S a-two)  0) 1] [else 0]))
                            (+ (* 60 (one-h a-one)) (one-m a-one) 
                               (cond[(> (one-s a-one)  0) 1] [else 0])) ))
                         (- (+ (* 60 (two-H a-two)) (two-M a-two) 
                               (cond[(> (two-S a-two)  0) 1] [else 0]))
                            (+ (* 60 (one-h a-one)) (one-m a-one) 
                               (cond[(> (one-s a-one)  0) 1] [else 0])) )]
          [else  (- 1440 (- (+ (* 60 (one-h a-one)) (one-m a-one) 
                               (cond[(> (one-s a-one)  0) 1] [else 0]))
                            (+ (* 60 (two-H a-two)) (two-M a-two) 
                               (cond[(> (two-S a-two)  0) 1] [else 0])) ))])]
    [else "Please input correct timestamps."]))
