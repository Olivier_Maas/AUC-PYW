;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-abbr-reader.ss" "lang")((modname DAD) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
; A List-of-Symbols is one of:
; - empty, or
; - (cons s los)
; where s is a Symbol and los is a List-of-Symbols.

; List-of-Symbols -> List-of-Symbols
; Takes a list of symbols and returns a similar list 
; but with adjacent duplicates removed.
(define (drop-adjacent-duplicates los)
  (cond
    [(empty? los) empty]
    [else
     (cond
       [(empty? (rest los)) (cons (first los) empty)]
       [(equal? (first los) (first (rest los))) 
        (drop-adjacent-duplicates (rest los))]
       [else (cons (first los) (drop-adjacent-duplicates (rest los)))])]))

(check-expect (drop-adjacent-duplicates '(a b b c c c d d d d e))
             '(a b c d e))
(check-expect (drop-adjacent-duplicates empty) empty)