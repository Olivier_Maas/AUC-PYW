# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 11:03:56 2014

@author: Olivier Maas
"""

def sort_tuples(lot):
    """Given a list of tuples of numbers return a list of the tuples 
    sorted by the last element of the tuples, e.g. [(1,2),(3,4,1),
    (7,5,6,9),(3,)] sorts to [(3,4,1),(1,2),(3,),(7,5,6,9)]. 
    Note that each tuple contains at least one number."""
    def getKey(item):
        return item[-1]
    return sorted(lot,key=getKey)

def sort_list(l):
    def getKey(item):
        return - len(item)
    return sorted(l,key=getKey)