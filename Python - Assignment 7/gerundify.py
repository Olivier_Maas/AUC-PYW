# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 12:25:17 2014

@author: Olivier Maas
"""

def gerundify(s):
    """Given a string, if its length is at least 3, add ='ing'= to its end. 
    Unless it already ends in 'ing', in which case add ='ly'= instead. 
    If the string length is less than 3, leave it unchanged. 
    Return the resulting string."""
    if s[-3:] == 'ing':
        return s + 'ly'
    elif len(s) >= 3:
        return s + 'ing'
    else:
        return s