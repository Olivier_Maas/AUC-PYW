# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 13:09:25 2014

@author: Olivier Maas
"""
from unittest import TestCase, main

from match_ends import match_ends

class MatchEndsTest(TestCase):
  def testWork(self):
    result = match_ends(['wow','no','aha','hahaha','ceec','aa','a'])
    self.assertEquals(result, 4)

if __name__ == '__main__':
  main()
