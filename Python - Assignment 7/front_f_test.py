# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 13:09:25 2014

@author: Olivier Maas
"""
from unittest import TestCase, main

from front_f import front_f

class FrontFTest(TestCase):
  def test1(self):
    result = front_f(['banana', 'apple', 'yam', 'fapple', 'fanana'] )
    self.assertEquals(result, ['fanana', 'fapple', 'apple', 'banana', 'yam'])

if __name__ == '__main__':
  main()
