# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 10:32:50 2014

@author: Olivier Maas
"""

def front_f(los):
    """Given a list of strings, return a list with the strings in sorted order, 
    except group all the strings that begin with 'f' first, 
    e.g. ['banana', 'apple', 'yam', 'fapple', 'fanana'] 
    would be sorted as ['fanana', 'fapple', 'apple', 'banana', 'yam']"""
    los.sort()
    x = 0
    for i in range(len(los)):
        if los[i][0] == "f":
            los = los[0:x] + [los[i]] + los[x:i] + los[i+1:]
            x = x+1
    return los