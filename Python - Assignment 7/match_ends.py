# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 10:21:11 2014

@author: Olivier Maas
"""

def match_ends(los):
    """Given a list of strings, return the count of the number of strings 
    where the string length is 2 or more 
    and the first and last chars of the string are the same."""
    x = 0
    for i in range(len(los)):
        if (los[i][0] == los[i][-1] and len(los[i]) > 1):
            x = x + 1
    return x