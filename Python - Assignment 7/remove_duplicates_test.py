# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 13:09:25 2014

@author: Olivier Maas
"""
from unittest import TestCase, main

from remove_duplicates import remove_duplicates

class RemoveDuplicatesTest(TestCase):
  def test1(self):
    result = remove_duplicates([1,2,2,3,3,3,4,5,5])
    self.assertEquals(result, [1,2,3,4,5])

if __name__ == '__main__':
  main()
