# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 13:09:25 2014

@author: Olivier Maas
"""
from unittest import TestCase, main

from fix_start import fix_start

class FixStartTest(TestCase):
  def testBubble(self):
    result = fix_start('bubble')
    self.assertEquals(result, 'bu**le')

  def testQ(self):
    result = fix_start('qqqq')
    self.assertEquals(result, 'q***')
    
if __name__ == '__main__':
  main()
