# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 13:09:25 2014

@author: Olivier Maas
"""
from unittest import TestCase, main

from both_ends import both_ends

class BothEndsTest(TestCase):
  def testBanana(self):
    result = both_ends('banana')
    self.assertEquals(result, 'bana')

  def test1(self):
    result = both_ends('q')
    self.assertEquals(result, '')
    
if __name__ == '__main__':
  main()
