# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 12:21:12 2014

@author: Olivier Maas
"""

def spoonerize(s1,s2):
    """A spoonerism is an error in speech where the first syllables of 
    two words are switched. The spoonerize function takes two words 
    as strings and returns a single string consisting of its two input strings
    separated by a space but with the first two letters of the words switched,
    e.g. spoonerise('jelly', 'beans') returns belly jeans"""
    l1 = s1[0]
    l2 = s2[0]
    return s2[0] + s1[1:] + " " + s1[0] + s2[1:]
    