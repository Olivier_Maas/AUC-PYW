# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 13:09:25 2014

@author: Olivier Maas
"""
from unittest import TestCase, main

from spoonerize import spoonerize

class SpoonerizeTest(TestCase):
  def test1(self):
    result = spoonerize('jelly', 'beans')
    self.assertEquals(result, 'belly jeans')

if __name__ == '__main__':
  main()
