# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 12:13:37 2014

@author: Olivier Maas
"""

def fix_start(s):
    """Given a string s, return a string where all occurences of its 
    first char have been changed to ='*'=, except do not change 
    the first char itself. e.g. ='bubble'= gives ='bu**le'= 
    Assume that the string is length 1 or more."""
    def remover(s,q):
        sn = ""
        for i in range(len(s)):
            if s[i] == q:
                sn = sn + '*'
            else:
                sn = sn + s[i]
        return sn
    return s[0] + remover(s[1:],s[0])