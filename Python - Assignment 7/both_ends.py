# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 12:08:01 2014

@author: Olivier Maas
"""

def both_ends(s):
    """Given a string s, return a string made of the first 2 
    and the last 2 chars of the original string, 
    so ='banana'= gives ='bana'= 
    However, if the string length is less than 2, 
    return instead the empty string."""
    if len(s) < 2:
        return ""
    else:
        return s[0:2] + s[-2:]