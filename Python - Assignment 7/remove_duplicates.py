# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 11:44:25 2014

@author: Olivier Maas
"""

def remove_duplicates(slon):
    """Given a list of whole numbers in increasing order, 
    return a list with duplicates removed, 
    e.g. [1,2,2,3,3,3,4,5,5] returns [1,2,3,4,5]"""
    if len(slon) == 0:
        return []
    elif len(slon) == 1:
        return [slon[0]]
    elif slon[0] == slon[1]:
        return remove_duplicates(slon[1:])
    else:
        return slon[0:1] + remove_duplicates(slon[1:])
