from unittest import TestCase, main

from triangle import classify_triangle

class ClassifyTriangleTest(TestCase):
  def testEquilateral(self):
    result = classify_triangle(1, 1, 1)
    self.assertEquals(result, 'equilateral')

  def testIsoceles(self):
    result = classify_triangle(2, 2, 1)
    self.assertEquals(result, 'isoceles')
    
  def testIsoceles2(self):
      result = classify_triangle(1,2,1)
      self.assertEquals(result, 'isoceles')

  def testScalene(self):
    result = classify_triangle(3, 4, 5)
    self.assertEquals(result, 'scalene')

  def testBadEquilateral(self):
    result = classify_triangle(1, 1, 99)
    self.assertNotEqual(result, 'equilateral')
    self.assertEquals(result, 'isoceles')

if __name__ == '__main__':
  main()
