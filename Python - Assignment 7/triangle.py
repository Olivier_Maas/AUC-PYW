def classify_triangle(a, b, c):
  """Classify a triangle based on the lengths of its three sides."""
  if a == b == c:
    return 'equilateral'
  elif (a == b) or (b == c) or (a == c):
    return 'isoceles'
  else:
    return 'scalene'
