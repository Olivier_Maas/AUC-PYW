# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 13:09:25 2014

@author: Olivier Maas
"""
from unittest import TestCase, main

from gerundify import gerundify

class GerundifyTest(TestCase):
  def testWork(self):
    result = gerundify('work')
    self.assertEquals(result, 'working')

  def testWorking(self):
    result = gerundify('working')
    self.assertEquals(result, 'workingly')

  def testWork(self):
    result = gerundify('no')
    self.assertEquals(result, 'no')

if __name__ == '__main__':
  main()
