# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 13:09:25 2014

@author: Olivier Maas
"""
from unittest import TestCase, main

from sort_tuples import sort_tuples

class SortTuplesTest(TestCase):
  def test1(self):
    result = sort_tuples([(1,2),(3,4,1),(7,5,6,9),(3,)])
    self.assertEquals(result, [(3, 4, 1), (1, 2), (3,), (7, 5, 6, 9)])

if __name__ == '__main__':
  main()
