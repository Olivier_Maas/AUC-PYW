#lang racket

(define sample-graph2
  '((1-1 1-2 2-1)
    (1-2 1-1 2-2 1-3)
    (1-3 1-2 2-3 1-4)
    (1-4 1-3 2-4)
    (2-1 1-1 2-2 3-1)
    (2-2 1-2 2-1 2-3 3-2)
    (2-3 1-3 2-2 2-4 3-3)
    (2-4 1-4 2-3 3-4)
    (3-1 2-1 3-2 4-1)
    (3-2 2-2 3-1 3-3 4-2)
    (3-3 2-3 3-2 3-4 4-3)
    (3-4 2-4 3-3 4-4)
    (4-1 3-1 4-2)
    (4-2 3-2 4-1 4-3)
    (4-3 3-3 4-4)
    (4-4 3-4 4-3)))

(define traversed '(1-2 2-3))

(define (neighbors n g)  
  (cond
    ((equal? n (first (first g)))
     (local 
       ((define (delist lol)
          (cond
            ((empty? lol) empty)
            (else (append (first lol) (delist (rest lol))))))
        (define neighbor-list 
          (delist (map (lambda (neigh) 
                         (if (member neigh traversed) 
                             empty (list neigh))) 
                       (rest (first g))))))
       (begin (set! traversed (append traversed neighbor-list))
              neighbor-list)))
    (else (neighbors n (rest g)))))

(define (delist lol)
  (cond
    ((empty? lol) empty)
    ((list? (first lol)) (append (delist (first lol)) (delist (rest lol))))
    (else (cons (first lol) (delist (rest lol))))))
