# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 16:03:45 2014

@author: Olivier Maas
"""

from math import pi, pow

class Shape(object):

    def __init__(self):
        pass

class Circle(Shape):

    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return pi * pow(self.radius, 2)

    def perimeter(self):
        return 2 * pi * self.radius

class Rectangle(Shape):

    def __init__(self, length, breadth):
        self.length = length
        self.breadth = breadth

    def area(self):
        return self.length * self.breadth
        
    def perimeter(self):
        return (self.length + self.breadth) * 2

class Square(Rectangle):

    def __init__(self, side):
        super(Square, self).__init__(side, side)
        
        
class Node:

    def __init__(self, x):
        self.left = None
        self.right = None
        self.x = x

    def insert(self, x):
        if x < self.x:
            if self.left is None:
                self.left = Node(x)
            else:
                self.left.insert(x)
        else:
            if self.right is None:
                self.right = Node(x)
            else:
                self.right.insert(x)