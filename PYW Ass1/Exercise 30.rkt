;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname |Exercise 30|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;Exercise 30
;String -> String
;Returns the first symbol from the given string.
(check-expect (string-first "Programming") "P")
(check-expect (string-first "First") "F")
(check-expect (string-first "abcd") "a")
(define (string-first word)
  (string-ith word 0))