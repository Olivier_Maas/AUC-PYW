;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname |Exercise 20|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;Exercise 20
;2 Strings -> String
;Joins two strings together, putting an underscore "_" between them.
(check-expect (string-join "Promote" "me") "Promote_me")
(check-expect (string-join "Zero" "underscores") "Zero_underscores")
(check-expect (string-join "Final" "check") "Final_check")
(define (string-join fst lst)
  (string-append fst
                 "_"
                 lst))