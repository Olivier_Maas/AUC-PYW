;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname |Exercise 34|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;Exercise 34
;String -> String
;Returns all but the last symbol of the given string.
(check-expect (string-remove-last "Programming") "Programmin")
(check-expect (string-remove-last "Test") "Tes")
(check-expect (string-remove-last "Woooo") "Wooo")
(define (string-remove-last word)
  (substring word 0 (- (string-length word) 1)))