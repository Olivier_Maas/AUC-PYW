#lang racket
(require htdp/testing)

; Exercise 385

(define sample-graph
  '((A B E P)
    (B E F)
    (C D)
    (D J)
    (E C F)
    (F D G)
    (G H)
    (H I K)
    (I J K)
    (J)
    (K L M N O)
    (L N O)
    (M N O)
    (N O)
    (O P)
    (P)))


(define sample-graph2
  '((1-1 1-2 2-1)
    (1-2 1-1 2-2 1-3)
    (1-3 1-2 2-3)
    (2-1 1-1 2-2 3-1)
    (2-2 1-2 2-1 2-3 3-2)
    (2-3 1-3 2-2 3-3)
    (3-1 2-1 3-2)
    (3-2 3-1 2-2 3-3)
    (3-3 2-3 3-2)))

; Node Node Graph -> [Maybe Path]
; finds a path from origination to destination in G
; if there is no path, the function produces false
(define (find-path origination destination G)
  (local ((define traversed empty)
          (define D destination)
          (define (neighbors n g)  
            (cond
              ((equal? n (first (first g))) (rest (first g)))
              (else (neighbors n (rest g)))))
          (define (find-path/list lo-Os) 
            (cond
              [(empty? lo-Os) false]
              [else (local ((define candidate (find-path2 (first lo-Os))))
                      (cond
                        [(boolean? candidate) (find-path/list (rest lo-Os))]
                        [else candidate]))]))
          (define (find-path2 origination)
            (cond
              [(symbol=? origination D) (list D)]
              [(member origination traversed) false]
              [else (begin (set! traversed (cons origination traversed))
                      (local ((define candidate 
                              (find-path/list (neighbors origination G))))
                      (cond
                        [(boolean? candidate) false]
                        [else (cons origination candidate)])))])))
    ; - IN -
    (find-path2 origination)))

(check-expect (find-path 'A 'P sample-graph)
              '(A B E F G H I K L N O P))
(check-expect (find-path 'A 'Q sample-graph) #f)
(check-expect (find-path '1-1 '3-3 sample-graph2)
              '(1-1 1-2 2-2 2-1 3-1 3-2 3-3))

(test)