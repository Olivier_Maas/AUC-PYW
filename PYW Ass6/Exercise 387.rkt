;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname |Exercise 387|) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
; Exercise 387


(define-struct transition [current key next])
(define-struct fsm [initial transitions final])
 
; A FSM is (make-fsm FSM-State [List-of 1Transition] FSM-State)
; A 1Transition is 
;   (make-transition FSM-State 1String FSM-State)
; A FSM-State is String
 
; data example: see exercise 100
(define fsm-a-bc*-d
  (make-fsm
   "AA"
   (list (make-transition "AA" "a" "BC")
         (make-transition "BC" "b" "BC")
         (make-transition "BC" "c" "BC")
         (make-transition "BC" "d" "DD"))
   "DD"))


; FSM String -> Boolean 
; does the given string match the regular expression expressed as fsm?
(define (fsm-match? a-fsm a-string)
  (local ((define command-list (explode a-string))
          (define (transer transition 1string)
            (if (eq? (transition-key transition) 1string) (transition-next transition) (transition-current transition)))
          (define (trans-check a-fsm lo1s)
            (cond
              ((or (empty? (fsm-transitions a-fsm)) (empty? lo1s)) (equal? (fsm-initial a-fsm) (fsm-final a-fsm)))
              ((equal? (fsm-initial a-fsm) (transition-current (first (fsm-transitions a-fsm))))
               (trans-check (make-fsm (transer (first (fsm-transitions a-fsm)) (first lo1s)) (fsm-transitions a-fsm) (fsm-final a-fsm)) (rest lo1s)))
              (else (trans-check (make-fsm (fsm-initial a-fsm) (rest (fsm-transitions a-fsm)) (fsm-final a-fsm)) (rest lo1s))))))
    (trans-check a-fsm command-list)))

(define fsm-ex
  (make-fsm
   "A" (list (make-transition "A" "a" "B")) "B"))