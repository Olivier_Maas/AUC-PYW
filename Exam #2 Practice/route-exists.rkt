#lang racket

;; A node is a symbol.

;; A pair is a list of two nodes:
;;   (cons S (cons T empty))
;; where S, T are symbols.

;; A simple-graph is a list of pairs:
;;   (listof pair).

(define SimpleG
  '((A B)
    (B C)
    (C E)
    (D E)
    (E B)
    (F F)))

;; route-exists? : node node simple-graph  ->  boolean
;; to determine whether there is a route from orig to dest in sg
(define (route-exists? orig dest sg)
  (local ((define traversed empty)
          ; route-exists? : node node ->  boolean
          (define (route-exists? orig dest)
            (cond [(symbol=? orig dest) true]
                  [else (route-exists? (neighbor orig sg) dest)]))
          ; neighbor : node simple-graph ->  node
          (define (neighbor a-node sg)
            (cond [(empty? sg) (error "neighbor: impossible")]
                  [else (cond [(symbol=? (first (first sg)) a-node)
                               (if (member (first (first sg)) traversed) 
                                   (neighbor a-node (rest sg)) 
                                   (begin (set! traversed 
                                                (cons (first (first sg))
                                                      traversed)) 
                                          (second (first sg))))]
                              [else (neighbor a-node (rest sg))])])))
    ; - IN -
    (route-exists? orig dest)))